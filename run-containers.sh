
sh ./stop-containers.sh

docker run -d -p 5000:5000 --name comprise-tt registry.gitlab.inria.fr/comprise/text_transformer
docker run -d --gpus all -p 5001:5000 --name comprise-st registry.gitlab.inria.fr/comprise/voice_transformation
docker run -it -v "/$(pwd)/server_url":/app/server_url -p 443:443 -p 8443:8443 -p 80:80 --link comprise-tt --link comprise-st --name comprise_personal_server compriseh2020/personal_server
