FROM ubuntu:18.04

MAINTAINER Gerrit Klasen "klasen@ascora.de"

# Basic system setup. This installs dependencies available in the Ubuntu 
# package manager needed by  the overall system and also from individual 
# (sub-)modules. If you need additional dependencies from apt-get, add 
# them here in the nested command below.

RUN echo "V3"
RUN apt-get update -y && \
    apt-get install software-properties-common -y && \
    add-apt-repository ppa:jonathonf/ffmpeg-4 -y && \
    add-apt-repository ppa:deadsnakes/ppa -y && \
    apt-get update -y && \
    apt-get upgrade -y && \
    apt-get install python3.7 python3.7-dev python3-pip -y && \
    apt-get install csh -y && \
    apt-get install zlib1g-dev automake autoconf unzip wget git sox cmake libtool subversion swig -y && \
    apt-get install libatlas-base-dev liblapack-dev libblas-dev -y && \
    apt-get install ffmpeg sox -y && \
    apt-get install libpcre3-dev -y && \
    apt-get install curl -y 

RUN python3.7 -m pip install --upgrade pip

# Also basic system requirements, but this time available in pip3 (Python 3
# package manager) instead of apt-get. If you need additional dependencies 
# here, add them to requirements.txt. Have a look at pypi.org, if you need to
# know if your version is available for pip3 and which version you need.

COPY ./requirements.txt /app/requirements.txt
WORKDIR /app
RUN pip3 install -r requirements.txt

# GPU
RUN wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/cuda-repo-ubuntu1804_10.0.130-1_amd64.deb
RUN apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/7fa2af80.pub
RUN dpkg -i cuda-repo-ubuntu1804_10.0.130-1_amd64.deb
RUN apt-get update
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y cuda-10-1

WORKDIR /app

# STT models from https://github.com/alphacep/vosk-apihttps://github.com/alphacep/vosk-api
# (concrete https://alphacephei.com/vosk/models), which will be used by flask server for
# speech recognition
COPY ./stt-models /app/stt-models
#COPY ./stt-models.1 /app/stt-models.1

# TTS 
COPY ./TTS /app/TTS 

RUN apt-get install espeak -y
ENV CFLAGS=-I/usr/local/lib/python3.7/dist-packages/numpy/core/include 
RUN python3.7 -m pip install /app/TTS/models/en/TTS-0.0.9.2-cp37-cp37m-linux_x86_64.whl
RUN python3.7 -m pip install /app/TTS/models/de/TTS-0.0.9.2-cp37-cp37m-linux_x86_64.whl
RUN python3.7 -m pip install /app/TTS/models/fr/TTS-0.0.9.2-cp37-cp37m-linux_x86_64.whl
RUN pip3 install pydub 

# PageKite
WORKDIR /app
COPY ./pagekite.py /app/ 
COPY ./.pagekite.rc /root/

# Gunicorn
RUN pip3 install gunicorn pymemcache 

# components needed at that time, returns result. Also copies cert keys for HTTPS:// support
COPY ./server.py /app/server.py

# SSL and certs
COPY ./comprise_cert.cer /app/comprise_cert.cer
COPY ./comprise_key.cer /app/comprise_key.cer

# Docker
RUN apt-get update -y 
RUN apt-get install docker.io -y

RUN add-apt-repository ppa:graphics-drivers/ppa

RUN distribution=$(. /etc/os-release;echo $ID$VERSION_ID) \
   && curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | apt-key add - \
   && curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | tee /etc/apt/sources.list.d/nvidia-docker.list
#RUN curl -s -L https://nvidia.github.io/libnvidia-container/experimental/$distribution/libnvidia-container-experimental.list | sudo tee /etc/apt/sources.list.d/libnvidia-container-experimental.list

RUN apt-get update
RUN apt-get install -y nvidia-docker2 nvidia-container-toolkit


###################################
# Insert your own component below #
# regarding installation, config, #
# misc. When you are done, copy   #
# files / folders needed to /app/ #
# Insert functions from your com- #
# ponent needed into server.py,   #
# which is the flask server. It   #
# connects smartphone APIs to     #
# backend services which execute  #
# your desired functionality      #
###################################


###################################
# Insert input above.             #
###################################

# Starting script for the server.
WORKDIR /app
COPY ./start.sh /app/start.sh

RUN chmod +x ./start.sh
CMD [ "sh", "./start.sh" ]


