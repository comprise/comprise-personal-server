
# flask_web/server.py
from flask import Flask, request, jsonify
from vosk import Model, KaldiRecognizer

from TTS.utils.synthesizer import Synthesizer
import io
import os
import uuid 
import wave
import base64
import json
import requests

app = Flask(__name__)
stt_models = {
}

synth_map = {}


def init():
    #Load STT Models on boot
    for tts_model in ['en', 'fr']:
        tts_checkpoint = "/app/TTS/models/" + tts_model +"/model.pth.tar"
        tts_config = "/app/TTS/models/" + tts_model + "/config.json"
        vocoder_checkpoint = "/app/TTS/models/" + tts_model +"/vocoder_model/model.pth.tar"
        vocoder_config = "/app/TTS/models/" + tts_model + "/vocoder_model/config.json"
        synthesizer = Synthesizer(tts_checkpoint, tts_config, vocoder_checkpoint, vocoder_config, False)
        synth_map[tts_model] = synthesizer
    if(os.path.getsize("/app/stt-models/chosen_stt.txt") > 0):
        try:
            chosen_stt_models = json.loads(open("/app/stt-models/chosen_stt.txt", "r").read())
            for stt_model in chosen_stt_models:
                 global stt_models
                 stt_models[stt_model['lang']] = Model("/app/stt-models/"+stt_model['descr'])
        except Exception as inst:
            print(inst)
    else:
        print("Worker loaded, no STT Models set yet.")

init()

@app.route('/')
def hello_world():
    return "Online"

@app.route("/url")
def url():
    file = open('/app/mysocket.url', 'rb')
    text = file.read()
    file.close()
    return text

@app.route("/available_stt_files")
def available_stt_models():
    with open('/app/stt-models/models') as f:
        stt_files = f.read().splitlines()
        return json.dumps(stt_files)

@app.route("/set_stt_models", methods=['POST'])
def set_stt_models():
    chosen_stt_models = request.get_json()['chosen_stt_models']
    for stt_model in chosen_stt_models:
        os.system("sh /app/stt-models/prepare_model.sh "+stt_model['appid']+" "+stt_model['modelid']+" "+stt_model['descr'])

    f = open("/app/stt-models/chosen_stt.txt", "w")
    f.write("".join(json.dumps(chosen_stt_models)))
    f.close()
    return "true"

@app.route("/privacy_driven_speech_transformation")
def privacy_driven_speech_transformation():

    pdst_url = "http://172.17.0.1:5001/vpc"

    # Read the content from an audio file
    pdst_input_file = 'userinput.wav'

    with open(pdst_input_file, mode='rb') as fp:
        pdst_content = fp.read()

    # Transformation parameters
    pdst_params = {'wgender': 'f',
          'cross_gender': 'same',
          'distance': 'plda',
          'proximity': 'dense',
          'sample-frequency': 16000}

    # Call the service
    pdst_response = requests.post(pdst_url, data=pdst_content, params=json.dumps(pdst_params))

    #print(pdst_response.content)

    # Save the result of the transformation in a new file
    pdst_result_file = 'transformed.wav'
    with open(pdst_result_file, mode='wb') as fp:
        fp.write(pdst_response.content)

    file = open('transformed.wav', 'rb')
    pdst_base64audio = base64.b64encode(file.read())
    file.close()
    return pdst_base64audio


@app.route("/privacy_driven_speech_transformation", methods=['POST'])
def privacy_driven_speech_transformation_post():

    pdst_url = "http://172.17.0.1:5001/vpc"

    # Read the content from an audio file

    base64audio = request.get_json()['audio']
    base64audio = base64audio.replace('data:audio/wav;base64,','')
    pdst_content = base64.b64decode(base64audio)

    # Transformation parameters
    pdst_params = {'wgender': 'f',
          'cross_gender': 'same',
          'distance': 'plda',
          'proximity': 'random',
          'sample-frequency': 16000}
   
    # Call the service
    pdst_response = requests.post(pdst_url, data=pdst_content, params=json.dumps(pdst_params))

    # Save the result of the transformation in a new file

    pdst_base64audio = base64.b64encode(pdst_response.content)
    return pdst_base64audio


@app.route("/privacy_driven_text_transformation", methods=['POST'])
def privacy_driven_text_transformation():

    plainText = request.get_json()['plainText']
    transformationMode = request.get_json()['transformationMode']

    pdtt_url = "http://172.17.0.1:5000/transform"

    # Transformation parameters
    pdtt_params = {'r': transformationMode}

    # Call the service
    headers = {'Content-Type': 'text/text; charset=utf-8'}
    response = requests.post(pdtt_url, data=plainText.encode('utf-8'), headers=headers, params=json.dumps(pdtt_params))

    return response.text


@app.route("/tts", methods=['POST'])
def tts():
    try:
        text = request.get_json()['text']
        lang = request.get_json()['lang']
        synthesizer = synth_map[lang]
        wavs = synthesizer.tts(text)
        out = io.BytesIO()
        synthesizer.save_wav(wavs, out)        

        out.seek(0)
        b64audio = base64.b64encode(out.read())
        return b64audio
    except Exception as inst:
        print(inst)

@app.route("/asr", methods=['POST'])
def asr():
    try:

        no_legacy = request.headers.get('x-no-legacy')

        base64audio = request.get_json()['audio']
        base64audio = base64audio.replace('data:audio/wav;base64,','')

        filename = uuid.uuid4()
        file = open(str(filename) + '.wav', 'wb')
        file.write(base64.b64decode(base64audio))
        file.close()

        if(no_legacy != "true"):
            file = open('userinput.wav', 'wb')
            file.write(base64.b64decode(base64audio))
            file.close()

        wf = wave.open(str(filename) + ".wav", "rb")

        model_lang = request.get_json()['lang']
        rec = KaldiRecognizer(stt_models.get(model_lang, "en"), wf.getframerate())

        result = ""
        while True:
            data = wf.readframes(200)
            if len(data) == 0:
                break
            if rec.AcceptWaveform(data):
                json_result = json.loads(rec.Result())
                result = result + " " + json_result['text']

        final = json.loads(rec.FinalResult())
        result = result + " " + final['text']

        return result
    except Exception as inst:
        print(inst)

if __name__ == "__main__":
    context = ('comprise_cert.cer','comprise_key.cer')
    app.run(host='0.0.0.0', port=8443, ssl_context=context)
