#!/bin/sh

rm -r ./stt-models/$3 || true 
wget -O ./stt-models/$3 "https://comprise-dev.tilde.com/v1alpha/applications/$1/models/ASR/$2?api_key=$3" &&
mv ./stt-models/$3 ./stt-models/$3.tar.gz
tar -xzvf ./stt-models/$3.tar.gz
rm ./stt-models/$3.tar.gz 

mkdir ./stt-models/$3 
mkdir ./stt-models/$3/am
mkdir ./stt-models/$3/conf
mkdir ./stt-models/$3/graph
mkdir ./stt-models/$3/graph/phones 
mkdir ./stt-models/$3/ivector

mv ./model/tdnn/final.mdl ./stt-models/$3/am/ 
mv ./model/tdnn/tree ./stt-models/$3/am/ 
mv ./model/tdnn_online/conf/mfcc.conf ./stt-models/$3/conf/
cp ./stt-models/model.conf ./stt-models/$3/conf/ 
cp -r ./model/graph/* ./stt-models/$3/graph/
cp -r ./model/rescore ./stt-models/$3
cp -r ./model/rnnlm ./stt-models/$3

mv ./model/graph_lookahead_arpa/disambig_tid.int ./stt-models/$3/graph/
mv ./model/graph_lookahead_arpa/HCLr.fst ./stt-models/$3/graph/
mv ./model/graph_lookahead_arpa/Gr.fst ./stt-models/$3/graph/
mv ./model/graph_lookahead_arpa/phones/word_boundary.int ./stt-models/$3/graph/phones/
mv ./model/tdnn_online/ivector_extractor/final.dubm ./stt-models/$3/ivector/
mv ./model/tdnn_online/ivector_extractor/final.ie ./stt-models/$3/ivector/
mv ./model/tdnn_online/ivector_extractor/final.mat ./stt-models/$3/ivector/
mv ./model/tdnn_online/ivector_extractor/global_cmvn.stats ./stt-models/$3/ivector/
mv ./model/tdnn_online/conf/online_cmvn.conf ./stt-models/$3/ivector/
mv ./model/tdnn_online/conf/splice.conf ./stt-models/$3/ivector/
rm -r ./model


