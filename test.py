from pydub import AudioSegment

song = AudioSegment.from_wav("./TTS/outputs/hey_there.wav")
song.export("./TTS/outputs/file.mp3", format="mp3")
