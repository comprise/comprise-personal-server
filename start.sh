#!/bin/sh

tr -dc A-Za-z0-9 </dev/urandom | head -c 13 > ./tmpurl
curl https://ipinfo.io/ip > ./serverip

sed -i "s/compriseexampleurl/$(cat ./tmpurl)/g" /root/.pagekite.rc
sed -i "s/mypublicip/127.0.0.1/g" /root/.pagekite.rc
sed -i "s/currentpath/root/g" /root/.pagekite.rc

echo "Following URL can be used within your server or desktop computer:

https://$(cat ./tmpurl).comprise-pagekite2.ascora.eu

(Server only:) If you prefer to insert IP address itself, use your servers IP and port:

https://$(cat ./serverip):8443" > ./server_url/personal_server_url.txt

python3 /app/pagekite.py --logfile=./server_url/pagekite.log &

gunicorn -w 1 -t 1 -b 0.0.0.0:8443 --certfile=comprise_cert.cer --keyfile=comprise_key.cer --timeout 1000 --reload --reload-extra-file /app/stt-models/chosen_stt.txt server:app
