# Install NodeJS

You will need to download **NodeJS and NPM** (as part of NodeJS) to start any work on **Ionic**. 

Visit the NodeJS homepage and download the latest LTS version for your operating system [here](https://nodejs.org/en/download/).

You should have **version 10.31 or newer** loaded!  

## Windows

Download the **installer** and execute it, follow the setup.

## MacOS

Download the **installer** and execute it, follow the setup.

As alternative:

Download and install **HomeBrew** from [here](https://docs.brew.sh/Installation) if you don’t have it.
 
Then, execute 

```bash
brew install nodejs
```

## Linux / Ubuntu

Follow instructions from [here](https://github.com/nodesource/distributions/blob/master/README.md).


## Verification

Please open a terminal and enter `npm -v` to confirm that NPM is installed correctly. 

<img src="./npm.png" width="500" height="50">