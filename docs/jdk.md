# Install Java Development Kit 8 (JDK 8)

Using **JDK 8** is also recommended, since it is needed when developing a COMPRISE App.

Downloads can be found [here](https://www.oracle.com/de/java/technologies/javase/javase-jdk8-downloads.html). Choose the file for your OS.

Installation instructions for your OS are available here:

- [Windows](https://docs.oracle.com/javase/8/docs/technotes/guides/install/windows_jdk_install.html#A1097936)
- [MacOS](https://www.oracle.com/java/technologies/javase/jdk-jre-macos-catalina.html) ([alternative](https://docs.oracle.com/javase/8/docs/technotes/guides/install/mac_jdk.html#A1096855))
- [Linux](https://docs.oracle.com/javase/8/docs/technotes/guides/install/linux_jdk.html#A1098871)

## Ubuntu

Insert following commands:


```bash
sudo apt-get install openjdk-8-jdk
sudo update-alternatives --config java (select JRE 8)
sudo update-alternatives --config javac (select JDK 8)
```

Add this to your .bash_profile (in addition to update-alternatives above):

```bash
export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
```