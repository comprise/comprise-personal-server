# Setup a Smartphone

For testing and development purposes at least one **physical Android device** is recommended. 

Emulators and other virtual testing environments mostly lack microphone and audio output support and will make it harder to use the comprise plugin functionality. 

The device must be in developer mode. To enable developer mode do the following:

1) On your device, open **Settings**
2) Scroll down and tap **About phone / tablet**
3) Search now for the **Build Number** field and tap it arount seven times until you receive a **You are now a developer** notification. 
4) Go back to **Settings**
5) You should find now a new menu entry, called **Developer options**. Tap that and search for **Enable USB Debugging** to activate it. 
6) Connect the device to your computer. Click to allow file transfer and then to allow USB debugging.
