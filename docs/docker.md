# Install Docker (Desktop)

You will also need **Docker installed** and running, if you want to setup your **own Personal Server** later.

**If you want to use Personal Server by a trusted third party provider, you do not need Docker to be installed**

Visit the instructions given by Docker for your OS:

### Docker Desktop (Windows / macOS)

- [MacOS](https://docs.docker.com/docker-for-mac/install/)
- [Windows](https://docs.docker.com/docker-for-windows/install/) ([alternative](https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository))

During installation, here for Windows, you will be asked if you want to enable HyperV-Features. Agree on that.

<img src="./docker_win1.png" height="200">

Navigate to the location where you installed Docker and try to start. 

Skip the tutorial, if you want. We will explain everything here.

Again: You potentially need to sign in. You may use "comprise-dev@web.de" and "comprise_h2020" as credentials.

<img src="./docker_win2.png" height="200">

If you will get the Alert above, you will need to access BIOS to activate HyperV-Features. 

Windows: If the problem perisists, it often is helpful to search for "Activate / Deactivate Windows Features" and turn "HyperV" off and on again, both guided with a re-start of the system.

### Docker (Ubuntu / Linux)

- [Ubuntu](https://docs.docker.com/engine/install/ubuntu/)

After installation, no additional steps are needed, you can pull a repository by `docker pull`.