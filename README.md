<img src="./comprise_personal_server.png" width="200" height="200">

Setting up the COMPRISE Personal Server is important to give your new app the 
ability to communicate with the web services of COMPRISE. 

# Requirements

- **GPU supporting** backend server or
- Windows / MacOS

# Installation

Before going through all the steps, you should install the [COMPRISE App Wizard](https://gitlab.inria.fr/comprise/comprise_app_wizard).

An installation & usage overview video can also be found on YouTube [here](https://www.youtube.com/watch?v=ZqNcCc-tdBk).

## Step 1 - Setup Docker on your personal environment

Follow the instructions given [here](https://gitlab.inria.fr/comprise/comprise-personal-server/-/blob/master/docs/docker.md) for Docker 

## Step 2 - Setup COMPRISE Personal Server
  
When you have the correct version of Docker for your OS installed, you 
need to make enter the following Docker command in your terminal: 

``` bash
docker pull registry.gitlab.inria.fr/comprise/text_transformer
docker pull registry.gitlab.inria.fr/comprise/voice_transformation
docker pull compriseh2020/personal_server
```

Alternatively, for `compriseh2020/personal_server`,  you also can pull this GitLab repository, navigate to the directory, make changes yourself, and build via:

``` bash
docker build -t compriseh2020/personal_server .
```

In case you chose the `docker pull` command for all repositories, that will cause Docker to download three containers with the COMPRISE Personal Server on your device, as well as the [Speech Transformation](https://gitlab.inria.fr/comprise/voice_transformation) and the [Text Transformation](https://gitlab.inria.fr/comprise/text_transformer)
Components.

**Repeat this regularily to check on updates.**

You will see multiple layers of the a Docker image downloaded and extracted in parallel. This will repeat for all of the three projects.

**Note ** Still, this will need some time. This will take its time depending on your internet connection. Please make sure that you have enough space for the image and the running docker container.
 
<img src="./step3.png" height="500">

If you have all the dependencies downloaded, you can start by: 

 
``` bash
docker run -d --gpus all -p 5000:5000 --name comprise-tt registry.gitlab.inria.fr/comprise/text_transformer
docker run -d --gpus all -p 5001:5000 --name comprise-st registry.gitlab.inria.fr/comprise/voice_transformation
docker run -it -v "/$(pwd)/server_url":/app/server_url -p 443:443 -p 8443:8443 -p 80:80 --link comprise-tt --name comprise_personal_server compriseh2020/personal_server
```
 
If you want to stop the containers (which also is important before you start a second time!), run:

``` bash
docker rm -f $(docker ps -a -q --filter name=comprise-tt ) | true
docker rm -f $(docker ps -a -q --filter name=comprise-st ) | true
docker rm -f $(docker ps -a -q --filter name=comprise_personal_server ) | true
```

If you decided to do a `git pull` for the Personal Server earlier, you also can execute `sh run-containers.sh` and `sh stop-containers` respectively.

The Docker container for the Personal Server is hosted at [DockerHub](https://hub.docker.com/repository/docker/compriseh2020/Personal_server) and is pushed to GitLab as well (this page). 
[Speech Transformation](https://gitlab.inria.fr/comprise/voice_transformation) and the [Text Transformation](https://gitlab.inria.fr/comprise/text_transformer) Components also are availible at GitLab.

Now check if everything went well, depending on your OS:

#### Windows / macOS

Open Docker Desktop via e.g. Notificationbar:

<img src="./step4.png" height="200">

... and open "Dashboard". You will see your Personal Server and others running.

<img src="./step5.png" height="200">

#### Linux / Ubuntu

If you enter  

```bash 
docker ps   
```
you will see your started Docker container with it’s container ID.  

<img src="./step11.png" height="50">


## Step 3 - Connect COMPRISE Personal Server with your App!

Remembering from **Step 2**, we executed following command to run our container: 

``` bash
docker run -d -p 5000:5000 --name comprise-tt registry.gitlab.inria.fr/comprise/text_transformer:latest
docker run -d --gpus all -p 5001:5000 --name comprise-st registry.gitlab.inria.fr/comprise/voice_transformation:latest
docker run -it -v "/$(pwd)/server_url":/app/server_url -p 443:443 -p 8443:8443 -p 80:80 --link comprise-tt --link comprise-st --name comprise_personal_server compriseh2020/personal_server:latest
```
 
The argument behind parameter `-v "/$(pwd)/server_url":/app/server_url` tells to copy the folder with your connection URL to your local file system.

The location is declared before the colon, so here path `/$(pwd)/server_url`, which will generate the folder **server_url** in the directory where you are currently running docker from. Of course, you can change the destination yourself.
 
It will contain a file called **personal_server_url.txt** with an exclusive URL, in the format `https://<customword>.comprise-pagekite2.ascora.eu` which you need to copy and to insert within [COMPRISE App Wizard](https://gitlab.inria.fr/comprise/comprise_app_wizard) to tell your future App the URL of your server!

Server only: If you do not want to use PageKite, you also can insert the IP address directly. This is also mentioned in the file.

Copy the marked URL above and continue with [COMPRISE App Wizard](https://gitlab.inria.fr/comprise/comprise_app_wizard)!


# Technical Component Integration

In general, most of the COMPRISE Smartphone Apps are Libaries containing RESTful APIs, which are connectet to this server. 

The APIs call their corresponding REST-Route within `server.py`, which is a Python Flask Server. It receives the call with optional parameters. 

Afterwards, the command to execute is processed directly in the server file or is forwarded to external components, like SimpleTextTransformation. 

The result always is delivered back to the Smartphone, which continues working with the callback. 

<img src="./step7.png" height="400">

## Step 1 - Inclusion of the component into Dockerfile

Open the repository of the solution of your choice. Copy the installation steps needed to setup the component and insert them into `Dockerfile` with the format 'RUN <my installation command>'. 

<img src="./step8.png" height="400">

Docker will read the Dockerfile like a recipe and executes one step after another, including the installation process just inserted. 

If you want to add additional configurations, files or file operations, insert them as well. 

At the end, copy the root folder of your new component to the Docker container's app folder, like so: `COPY ./mySolution /app/` Open Dockerfile to find additional examples.

## Step 2 - Connect component to Flask Server

The flask server, which is `server.py`, connects Smartphone APIs with our server code. Like below, you will need to define a route like `/my_method_call`, including the method type (`GET/POST/PUT/DELETE/PATCH`).

The `request` param contains the json body you transmitted as a parameter, including params needed like strings, objects, or others.

`transform_text(...)` is a method imported in the file from our COMPRISE research implementation, whose result will be returned to the phone.

<img src="./step9.png" height="100">

## Step 3 - Create a client library which matches the Flask server

Run `git clone https://gitlab.inria.fr/comprise/comprise-example-library.git` to clone a dummy client library for COMPRISE.

Run `npm i` to have all dependencies needed installed.

Navigate to `./src/index.ts` to implement the function you would like to use later in your application. This function also contains the REST call to the Personal Server.

<img src="./step10.png" height="300">

Go to `./package.json` and increase the version number, run `npm publish` to publish your library. Your app then can install it via `npm i --save mypackage@versionnumber`




# Data Collection integration

The server can collect data in form of server requests and responses for scientific evaluation. Multiple steps need to be done to enable it. 

All data needs to be stored to a hard drive, that's why the docker container needs to bind a local folder from the system it's running on. Please note that this may require a large amount of storage depending on the time the data is recorded and the amount of requests that are going in. 

The command to start the Docker container with a local folder mounted looks like this: 


``` bash
 docker run -it --link comprise-tt -v ~/server_url:/app/server_url   --mount type=bind,source=/Users/andreflathmann/evaluation,target=/var/evaluation  -p 443:443 -p 8443:8443 -p 80:80  <ID of the docker image>
 ```

 While the source path should be a folder on your system, the target directory needs to be /var/evaluation. 

 To give the data some structure, additional HTTP headers need to be sent using the Comprise SDK. 

 It could look like this: 

```typescript
    // Set the three HTTP headers 
     makeHeaders(http) {
        http.setHeader('x-app-id', this.app_id); // Can be a constant string for the app ("comprise_notes", for example)
        http.setHeader('x-session-id', this.session_id); // Session id uuid. Should be new generated on each user action. (Could be done on each voice input, for example) 
        http.setHeader('x-user-id', this.user_id); // User id, uuid. Should be generated on first start of the app and stored for re-use then
    }

    // Prepare http object 
    makeHTTP() {
        const http = this.http; 
        this.makeHeaders(http);
        return http;
```

Calling one of the SDK functions using this preparation would look like this then: 

```typescript
        const http = this.makeHTTP();

        streamRecording(this.appStateService.currentLang, http, false).then((emitter: any) => {
            ...
 
```

## File Structure

The recorded data will be stored in the given mount source path given to the docker run command. The structure will be: 

<your_source_folder>/<app_id>/<session_id>/<user_id> 

On every SDK related request to the server files will be generated that will contain the request to the server and the response from the server. 



